# [batchasync.js](https://git.coolaj86.com/coolaj86/batchasync.js)

Like calling `map()` and resolving with `Promise.all()`,
but handling a bounded number of items at any given time.

Want to read about it?

- [Batching async requests in &lt; 50 lines of VanillaJS](https://coolaj86.com/articles/batching-async-requests-50-lines-of-vanilla-js/)

# Install

```bash
npm install --save batchasync
```

# Usage

```js
// Browsers
var batchAsync = window.batchAsync;
```

```js
// Node
var batchAsync = require('batchasync').batchAsync;
```

```js
var batchSize = 4;
var things = ['apples', 'bananas', 'pears'];

function doStuff() {
	// ... go fetch things
}

batchAsync(batchSize, things, doStuff).then(function(results) {
	// all results, in order, just like Promise.all()
	console.log(results[0]);
});
```

## A note on `undefined`

Returning `undefined` will cause an exception to be thrown,
and the Promise to be rejected.

This is on purpose - because `undefined` is indistinguishable from a skipped Promise.

Return `null` instead.

**Example (bad)**:

```js
function doStuff(thing) {
	// Oops! forgot to return
	request('https://searchallthe.pictures/api/' + thing);
}
```

**Example (good)**:

```js
function doStuff(thing) {
	return request('https://searchallthe.pictures/api/' + thing);
}
```

**Example (good)**:

```js
function doStuff(thing) {
	return null;
}
```
